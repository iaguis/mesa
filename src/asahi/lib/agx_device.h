/*
 * Copyright (C) 2021 Alyssa Rosenzweig <alyssa@rosenzweig.io>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __AGX_DEVICE_H
#define __AGX_DEVICE_H

#include "util/sparse_array.h"
#include "util/simple_mtx.h"
#include "io.h"
#include "agx_formats.h"

#if __APPLE__
#include <mach/mach.h>
#include <IOKit/IOKitLib.h>
#else
#include "drm-uapi/asahi_drm.h"
#endif

// TODO: this is a lie right now
static const uint64_t AGX_SUPPORTED_INCOMPAT_FEATURES =
   DRM_ASAHI_FEAT_MANDATORY_ZS_COMPRESSION;


enum agx_dbg {
   AGX_DBG_TRACE = BITFIELD_BIT(0),
   AGX_DBG_DEQP  = BITFIELD_BIT(1),
   AGX_DBG_NO16  = BITFIELD_BIT(2),
   AGX_DBG_DIRTY  = BITFIELD_BIT(3),
   AGX_DBG_PRECOMPILE  = BITFIELD_BIT(4),
   AGX_DBG_PERF  = BITFIELD_BIT(5),
   AGX_DBG_NOCOMPRESS = BITFIELD_BIT(6),
};

struct agx_device_id {
   char name[64];

   int generation;
   char variant;
   int revision;
   uint32_t chip_id;

   uint64_t feat_compat;
   uint64_t feat_incompat;
};

/* How many power-of-two levels in the BO cache do we want? 2^14 minimum chosen
 * as it is the page size that all allocations are rounded to
 */
#define MIN_BO_CACHE_BUCKET (14) /* 2^14 = 16KB */
#define MAX_BO_CACHE_BUCKET (22) /* 2^22 = 4MB */

/* Fencepost problem, hence the off-by-one */
#define NR_BO_CACHE_BUCKETS (MAX_BO_CACHE_BUCKET - MIN_BO_CACHE_BUCKET + 1)

struct agx_device {
   void *memctx;
   uint32_t debug;

   struct agx_device_id id;

   /* XXX What to bind to? I don't understand the IOGPU UABI */
   struct agx_command_queue queue;
#if __APPLE__
   struct agx_bo cmdbuf, memmap;
#endif
   uint64_t next_global_id, last_global_id;

   /* Device handle */
#if __APPLE__
   io_connect_t fd;
#else
   int fd;
#endif
   struct renderonly *ro;

   pthread_mutex_t bo_map_lock;
   struct util_sparse_array bo_map;

   struct {
      simple_mtx_t lock;

      /* List containing all cached BOs sorted in LRU (Least Recently Used)
       * order so we can quickly evict BOs that are more than 1 second old.
       */
      struct list_head lru;

      /* The BO cache is a set of buckets with power-of-two sizes.  Each bucket
       * is a linked list of free panfrost_bo objects.
       */
      struct list_head buckets[NR_BO_CACHE_BUCKETS];

      /* Current size of the BO cache in bytes (sum of sizes of cached BOs) */
      size_t size;

      /* Number of hits/misses for the BO cache */
      uint64_t hits, misses;
   } bo_cache;
};

bool
agx_open_device(void *memctx, struct agx_device *dev);

void
agx_close_device(struct agx_device *dev);

static inline struct agx_bo *
agx_lookup_bo(struct agx_device *dev, uint32_t handle)
{
   return util_sparse_array_get(&dev->bo_map, handle);
}

#ifdef __APPLE__
struct agx_bo
agx_shmem_alloc(struct agx_device *dev, size_t size, bool cmdbuf);

void
agx_shmem_free(struct agx_device *dev, unsigned handle);
#endif

uint64_t
agx_get_global_id(struct agx_device *dev);

struct agx_command_queue
agx_create_command_queue(struct agx_device *dev);

#if __APPLE__
void
agx_submit_cmdbuf(struct agx_device *dev, struct agx_bo *cmdbuf, unsigned mappings, uint64_t scalar);
#else
int
agx_submit_cmdbuf(struct agx_device *dev, struct drm_asahi_cmdbuf *c);
#endif

void
agx_wait_queue(struct agx_command_queue queue);

void
agx_bo_mmap(struct agx_bo *bo);

const char *
agx_get_revision_string(const struct agx_device_id *id);

#endif
